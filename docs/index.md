# Bem vindo ao MkDocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Comandos MkDocs

* `mkdocs new [dir-name]` - Criar um novo projeto.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
