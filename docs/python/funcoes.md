# Funções Úteis


## Funções Matemáticas
```python
def soma(a: int, b: int): -> int
    return a + b
```
!!! note "Nota"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Erit enim instructus ad mortem contemnendam, ad exilium, ad ipsum etiam dolorem. Quam tu ponis in verbis, ego positam in re putabam. Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam. Inde sermone vario sex illa a Dipylo stadia confecimus. Duo Reges: constructio interrete. Eaedem res maneant alio modo. Nam quid possumus facere melius? 




## Funções Aleatórias
